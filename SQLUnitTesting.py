import unittest
import sys
from io import StringIO 
from sqlScriptCreator import *

class SQLTest(unittest.TestCase):
    def testSQLTable(self):
        json = [
        {
        "ControlAccountID": "XYJM-MFPH-RXRG",
        "ReportingPeriodID": 9,
        "Value_Dollars": 8558121,
        "Value_Dollars_LAB": 3383139,
        "Value_Dollars_MAT": 2212965,
        "Value_Dollars_ODC": 946404,
        "Value_Dollars_SUB": 2015613,
        "Value_Hours": 30720
        }
        ]

        assertion = ["ControlAccountID" ,"ReportingPeriodID", "Value_Dollars", 
                    "Value_Dollars_LAB", "Value_Dollars_MAT", "Value_Dollars_ODC",
                    "Value_Dollars_SUB", "Value_Hours"]
        
        self.assertListEqual(CreateFields(json).writeColumnsSql(), assertion)

        json = [
        {
        "Level": 1,
        "ID": "TOTAL",
        "Name": "TOTAL"
        },
        {
            "Level": 2,
            "ID": "1.1",
            "Name": "Subsystem 1.1",
            "ParentID": "TOTAL"
        }
        ]
        assertion = ["Level", "ID", "Name","ParentID"]
        self.assertListEqual(CreateFields(json).writeColumnsSql(), assertion)

        json = [
        {
            "ActualFinishDate": "2029-03-08",
            "ActualStartDate": "2020-09-09",
            "BaselineDuration": 2127.0,
            "BaselineFinishDate": "2029-03-08",
            "BaselineStartDate": "2020-09-09",
            "CalculatedPercentComplete": 0.0,
            "CalendarID": "NGC 5 Day Workweek thru 2040",
            "CurrentDuration": 2127.0,
            "CurrentFinishDate": "2029-03-08",
            "CurrentStartDate": "2020-09-09",
            "EarlyFinishDate": "0001-01-01",
            "EarlyStartDate": "0001-01-01",
            "FinishVarianceDuration": 0.0,
            "FreeFloatDuration": 0.0,
            "LateFinishDate": "0001-01-01",
            "LateStartDate": "0001-01-01",
            "OnCriticalPath": True,
            "OnDrivingPath": False,
            "RemainingDuration": 2127.0,
            "StartVarianceDuration": 0.0,
            "TaskID": "SK-S363_MB-SPD",
            "TotalFloatDuration": -397.0
        },
        {
            "BaselineDuration": 0.0,
            "CalculatedPercentComplete": 0.0,
            "CalendarID": "NGC 5 Day Workweek thru 2040",
            "CurrentDuration": 526908.0,
            "CurrentFinishDate": "0001-01-01",
            "CurrentStartDate": "0001-01-01",
            "EarlyFinishDate": "0001-01-01",
            "EarlyStartDate": "0001-01-01",
            "FreeFloatDuration": 0.0,
            "LateFinishDate": "0001-01-01",
            "LateStartDate": "0001-01-01",
            "OnCriticalPath": True,
            "OnDrivingPath": False,
            "RemainingDuration": 0.0,
            "TaskID": "SK-S363_MB-SPD.1.15",
            "TotalFloatDuration": 0.0
        },
        {
            "BaselineDuration": 0.0,
            "BaselineFinishDate": "2020-09-09",
            "BaselineStartDate": "2020-09-09",
            "CalculatedPercentComplete": 0.0,
            "CalendarID": "NGC 5 Day Workweek thru 2040",
            "CurrentDuration": 0.0,
            "CurrentFinishDate": "2020-09-09",
            "CurrentStartDate": "2020-09-09",
            "EarlyFinishDate": "2020-09-09",
            "StopDate": "2020-09-09",
            "FinishVarianceDuration": 0.0,
            "FreeFloatDuration": 0.0,
            "LateFinishDate": "2026-01-19",
            "LateStartDate": "2026-01-19",
            "OnCriticalPath": False,
            "OnDrivingPath": False,
            "PhysicalPercentComplete": 0.0,
            "RemainingDuration": 0.0,
            "StartVarianceDuration": 0.0,
            "TaskID": "FSTSUPGIV675",
            "TotalFloatDuration": 1342.0
        }
        ]

        jsonList = CreateFields(json).writeColumnsSql() 
        assertion = ["ActualFinishDate","ActualStartDate","BaselineDuration",
                     "BaselineFinishDate","BaselineStartDate","CalculatedPercentComplete",
                     "CalendarID", "CurrentDuration","CurrentFinishDate",
                     "CurrentStartDate", "EarlyFinishDate","EarlyStartDate", 
                     "FinishVarianceDuration", "FreeFloatDuration","LateFinishDate",
                     "LateStartDate", "OnCriticalPath","OnDrivingPath", 
                     "RemainingDuration", "StartVarianceDuration", "StopDate",
                     "PhysicalPercentComplete","TaskID","TotalFloatDuration"]
       
        self.assertListEqual(sorted(jsonList) , sorted(assertion))


    def testFileBuild(self):
        directory = ["File.txt","Test.json", "Stuff.json", "UnrelatedFile.jso", "FinalFile.json"]
        assertion = ["Test.json","Stuff.json","FinalFile.json"]
        self.assertTrue(JsonList(directory).buildJsonList() == assertion)

        directory = ["File.txt", "Stuff.dir", "UnrelatedFile.gdb", "FinalFile.java"]
        self.assertTrue(JsonList(directory).buildJsonList() == [])

    def testSQL(self):
        capturedOutput = StringIO()
        sys.stdout = capturedOutput

        assertion = "DECLARE @JSON varchar(max)\
                    SELECT @JSON = BulkColumn \
                    FROM OPENROWSET (BULK 'C:\Data\Tasks.json', SINGLE_CLOB) IMPORT\
                    SELECT * INTO CA_Tasks_STG\
                    FROM OPENJSON(@JSON)\
                    WITH(\
                            [ID] VARCHAR (50),\
                            [Name] VARCHAR (50),\
                            [TaskTypeID] VARCHAR (50),\
                            [TaskSubtypeID] VARCHAR (50),\
                            [TaskPlanningLevelID] VARCHAR (50),\
                            [WBSElementID] VARCHAR (50),\
                            [OBSElementID] VARCHAR (50),\
                            [ControlAccountID] VARCHAR (50),\
                            [WorkPackageID] VARCHAR (50),\
                            [IMPElementID] VARCHAR (50),\
                            [SOWReference] VARCHAR (50),\
                            [SubcontractorReference] VARCHAR (50),\
                            [EarnedValueTechniqueID] VARCHAR (50),\
                            [OtherEarnedValueTechnique]	 VARCHAR (50),\
                            [SourceSubprojectReference]	VARCHAR (50),\
                            [SourceTaskReference]	VARCHAR (50),\
                            [Comments] VARCHAR (50)\
                        )\
                        SELECT * FROM CA_Tasks_STG"

        fields = ["ID" ,"Name", "TaskTypeID", "TaskSubtypeID", 
                  "TaskPlanningLevelID", "WBSElementID", "OBSElementID",
                  "ControlAccountID", "WorkPackageID", "IMPElementID",
                  "SOWReference","SubcontractorReference","EarnedValueTechniqueID",
                  "OtherEarnedValueTechnique","SourceSubprojectReference",
                  "SourceTaskReference", "Comments"]
        
        CreateSQL("Tasks").writer(fields, 50)
        
        sys.stdout = sys.__stdout__  
        stringTest = capturedOutput.getvalue().replace(' ', '').replace('\n', '').replace('\t','')
        assertion = assertion.replace(' ','').replace('\t','')

        self.assertMultiLineEqual(assertion, stringTest)


if __name__ == "__main__":
    unittest.main()