DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\TaskCustomFieldValues.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_TaskCustomFieldValues_STG
FROM OPENJSON(@JSON)
WITH( 
	[TaskID] VARCHAR (250), 
	[CustomFieldID] VARCHAR (250), 
	[Value] VARCHAR (2000)
)
SELECT * FROM CA_TaskCustomFieldValues_STG