CREATE TABLE CA_WorkPackages_STG(
	[IsPlanningPackage]			varchar(250),
	[ID]							varchar(250),
	[Name]						varchar(250),
	[BaselineStartDate]			varchar(250),
	[BaselineEndDate]				varchar(250),
	[ForecastStartDate]			varchar(250),
	[ForecastEndDate]				varchar(250),
	[ActualStartDate]				varchar(250),
	[ActualEndDate]				varchar(250),
	[EarnedValueTechniqueID]		varchar(250),
	[OtherEarnedValueTechnique]	varchar(2000),
	[ControlAccountID]			varchar(250)
);
