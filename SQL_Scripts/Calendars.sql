DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\Calendars.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_Calendars_STG
FROM OPENJSON(@JSON)
WITH( 
	[ID] VARCHAR (250), 
	[Name] VARCHAR (250),
	[Comments]	varchar(2000)
)
SELECT * FROM CA_Calendars_STG