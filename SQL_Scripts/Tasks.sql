DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\Tasks.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_Tasks_STG
FROM OPENJSON(@JSON)
WITH( 
	[ID] VARCHAR (250),
	[Name] VARCHAR (250),
	[TaskTypeID] VARCHAR (250),
	[TaskSubtypeID] VARCHAR (250),
	[TaskPlanningLevelID] VARCHAR (250),
	[WBSElementID] VARCHAR (250),
	[OBSElementID] VARCHAR (250),
	[ControlAccountID] VARCHAR (250),
	[WorkPackageID] VARCHAR (250),
	[IMPElementID] VARCHAR (250),
	[SOWReference] VARCHAR (2000),
	[SubcontractorReference] VARCHAR (2000),
	[EarnedValueTechniqueID] VARCHAR (250),
	[OtherEarnedValueTechnique]	 VARCHAR (2000),
	[SourceSubprojectReference]	VARCHAR (2000),
	[SourceTaskReference]	VARCHAR (2000),
	[Comments] VARCHAR (2000)
)
SELECT * FROM CA_Tasks_STG