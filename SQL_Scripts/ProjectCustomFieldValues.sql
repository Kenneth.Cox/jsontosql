DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ProjectCustomFieldValues.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ProjectCustomFieldValues_STG
FROM OPENJSON(@JSON)
WITH( 
	[CustomFieldID] VARCHAR (250), 
	[Value] VARCHAR (2000)
)
SELECT * FROM CA_ProjectCustomFieldValues_STG