DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\WBS.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_WBS_STG
FROM OPENJSON(@JSON)
WITH( 
	[Level] VARCHAR (250), 
	[ID] VARCHAR (250), 
	[Name] VARCHAR (2000), 
	[ParentID] VARCHAR (250)
)
SELECT * FROM CA_WBS_STG