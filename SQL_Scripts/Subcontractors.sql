DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\Subcontractors.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_Subcontractors_STG
FROM OPENJSON(@JSON)
WITH( 
	[ID] VARCHAR (250), 
	[Name] VARCHAR (1000)
)
SELECT * FROM CA_Subcontractors_STG