DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\Resources.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_Resources_STG
FROM OPENJSON(@JSON)
WITH( 
	[ID] VARCHAR (250), 
	[Name] VARCHAR (250), 
	[ElementOfCostID] VARCHAR (250),
	[Comments] VARCHAR (2000) 
)
SELECT * FROM CA_Resources_STG