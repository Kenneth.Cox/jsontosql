DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\TaskRelationships.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_TaskRelationships_STG
FROM OPENJSON(@JSON)
WITH( 
	[PredecessorTaskID] VARCHAR (250), 
	[SuccessorTaskID] VARCHAR (250), 
	[RelationshipTypeID] VARCHAR (250), 
	[LagDuration] VARCHAR (250), 
	[LagCalendarID] VARCHAR (250)
)
SELECT * FROM CA_TaskRelationships_STG