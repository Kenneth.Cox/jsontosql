DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ReportingCalendar.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ReportingCalendar_STG
FROM OPENJSON(@JSON)
WITH( 
	[ID] VARCHAR (250), 
	[StartDate] VARCHAR (250), 
	[EndDate] VARCHAR (250), 
	[WorkingHours] VARCHAR (250)
)
SELECT * FROM CA_ReportingCalendar_STG