DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ResourceAssignments.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ResourceAssignments_STG
FROM OPENJSON(@JSON)
WITH( 
	[ResourceID] VARCHAR (250), 
	[TaskID] VARCHAR (250), 
	[Budget_AtCompletion_Dollars] VARCHAR (250), 
	[Budget_AtCompletion_Hours] VARCHAR (250), 
	[Actual_ToDate_Dollars] VARCHAR (250), 
	[Actual_ToDate_Hours] VARCHAR (250), 
	[PhysicalPercentComplete] VARCHAR (250), 
	[Estimate_ToComplete_Dollars] VARCHAR (250), 
	[Estimate_ToComplete_Hours] VARCHAR (250)
)
SELECT * FROM CA_ResourceAssignments_STG