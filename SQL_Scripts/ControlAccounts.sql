DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ControlAccounts.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ControlAccounts_STG
FROM OPENJSON(@JSON)
WITH( 
	[IsSummaryLevelPlanningPackage] VARCHAR (250),
	[ID] VARCHAR (250), 
	[Name] VARCHAR (2000), 
	[BaselineStartDate] VARCHAR (250), 
	[BaselineEndDate] VARCHAR (250), 
	[ForecastStartDate] VARCHAR (250), 
	[ForecastEndDate] VARCHAR (250), 
	[ActualStartDate] VARCHAR (250),
    [ActualEndDate] VARCHAR (250),
	[ManagerName] VARCHAR (250), 
	[WBSElementID] VARCHAR (250), 
	[OBSElementID] VARCHAR (250)
)
SELECT * FROM CA_ControlAccounts_STG