DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ReprogrammingAdjustments.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ReprogrammingAdjustments_STG
FROM OPENJSON(@JSON)
WITH( 
	[ControlAccountID] VARCHAR (250), 
	[ReprogSVA_Dollars] VARCHAR (250), 
	[ReprogCVA_Dollars] VARCHAR (250), 
	[ReprogBA_Dollars] VARCHAR (250), 
	[ReprogSVA_Hours] VARCHAR (250), 
	[ReprogCVA_Hours] VARCHAR (250), 
	[ReprogBA_Hours] VARCHAR (250)
)
SELECT * FROM CA_ReprogrammingAdjustments_STG