DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ResourceCustomFieldValues.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ResourceCustomFieldValues_STG
FROM OPENJSON(@JSON)
WITH( 
	[ResourceID] VARCHAR (250), 
	[CustomFieldID] VARCHAR (250), 
	[Value] VARCHAR (2000)
)
SELECT * FROM CA_ResourceCustomFieldValues_STG