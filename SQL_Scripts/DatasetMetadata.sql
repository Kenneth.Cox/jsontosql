DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\DatasetMetadata.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_DatasetMetadata_STG
FROM OPENJSON(@JSON)
WITH( 
	[SecurityMarking] VARCHAR (250),
	[DistributionStatement] VARCHAR (2000), 
	[ReportingPeriodID] VARCHAR (250), 
	[ContractorName] VARCHAR (250), 
	[ContractorIDCodeTypeID] VARCHAR (250), 
	[ContractorIDCode] VARCHAR (250), 
	[ContractorAddress_Street] VARCHAR (250), 
	[ContractorAddress_City] VARCHAR (250), 
	[ContractorAddress_State] VARCHAR (250), 
	[ContractorAddress_Country] VARCHAR (250), 
	[ContractorAddress_ZipCode] VARCHAR (250), 
	[PointOfContactName] VARCHAR (250), 
	[PointOfContactTitle] VARCHAR (250), 
	[PointOfContactTelephone] VARCHAR (250), 
	[PointOfContactEmail] VARCHAR (254), 
	[ContractName] VARCHAR (250), 
	[ContractNumber] VARCHAR (250), 
	[ContractType] VARCHAR (250), 
	[ContractTaskOrEffortName] VARCHAR (250), 
	[ProgramName] VARCHAR (250), 
	[ProgramPhase] VARCHAR (20), 
	[EVMSAccepted] VARCHAR (250), 
	[EVMSAcceptanceDate] VARCHAR (250)
)
SELECT * FROM CA_DatasetMetadata_STG