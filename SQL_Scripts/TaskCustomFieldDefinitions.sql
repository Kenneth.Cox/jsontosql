DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\TaskCustomFieldDefinitions.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_TaskCustomFieldDefinitions_STG
FROM OPENJSON(@JSON)
WITH( 
	[CustomFieldID] VARCHAR (250), 
	[Name] VARCHAR (250), 
	[Comments] VARCHAR (2000)
)
SELECT * FROM CA_TaskCustomFieldDefinitions_STG