DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\TaskConstraints.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_TaskConstraints_STG
FROM OPENJSON(@JSON)
WITH( 
	[TaskID] VARCHAR (250), 
	[ConstraintTypeID] VARCHAR (250), 
	[OtherConstraintType] VARCHAR (2000),
	[ConstraintDate] VARCHAR (250)
)
SELECT * FROM CA_TaskConstraints_STG