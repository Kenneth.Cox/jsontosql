DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\DatasetConfiguration.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_DatasetConfiguration_STG
FROM OPENJSON(@JSON)
WITH( 
	[NonAdd_OH] VARCHAR (250), 
	[NonAdd_COM] VARCHAR (250), 
	[NonAdd_GA] VARCHAR (250), 
	[ToDate_TimePhased] VARCHAR (250), 
	[Detail_HasDirectValues] VARCHAR (250), 
	[Detail_HasIndirectValues] VARCHAR (250), 
	[BCWS_ToDate_ByWorkPackage] VARCHAR (250), 
	[BCWS_ToDate_HasElementOfCostValues] VARCHAR (250), 
	[BCWP_ToDate_ByWorkPackage] VARCHAR (250), 
	[BCWP_ToDate_HasElementOfCostValues] VARCHAR (250), 
	[ACWP_ToDate_ByWorkPackage] VARCHAR (250), 
	[ACWP_ToDate_HasElementOfCostValues] VARCHAR (250), 
	[BCWS_ToComplete_ByWorkPackage] VARCHAR (250), 
	[BCWS_ToComplete_HasElementOfCostValues] VARCHAR (250), 
	[EST_ToComplete_ByWorkPackage] VARCHAR (250), 
	[EST_ToComplete_HasElementOfCostValues] VARCHAR (250)
)
SELECT * FROM CA_DatasetConfiguration_STG