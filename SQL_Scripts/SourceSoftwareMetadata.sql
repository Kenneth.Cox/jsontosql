DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\SourceSoftwareMetadata.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_SourceSoftwareMetadata_STG
FROM OPENJSON(@JSON)
WITH( 
	[Data_SoftwareName] VARCHAR (250), 
	[Data_SoftwareVersion] VARCHAR (250), 
	[Data_SoftwareCompanyName] VARCHAR (250),
	[Data_SoftwareComments] VARCHAR (2000), 
	[Export_SoftwareName] VARCHAR (250), 
	[Export_SoftwareVersion] VARCHAR (250), 
	[Export_SoftwareCompanyName] VARCHAR (250),
	[Export_SoftwareComments] VARCHAR (2000)
)
SELECT * FROM CA_SourceSoftwareMetadata_STG