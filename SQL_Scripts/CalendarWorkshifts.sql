DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\CalendarWorkshifts.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_CalendarWorkshifts_STG
FROM OPENJSON(@JSON)
WITH( 
	[CalendarID] VARCHAR (250), 
	[Ordinal] VARCHAR (250),
	[MondayWorkHours] VARCHAR (250), 
	[TuesdayWorkHours] VARCHAR (250), 
	[WednesdayWorkHours] VARCHAR (250), 
	[ThursdayWorkHours] VARCHAR (250), 
	[FridayWorkHours] VARCHAR (250), 
	[SundayWorkHours] VARCHAR (250), 
	[SaturdayWorkHours] VARCHAR (250)
)
SELECT * FROM CA_CalendarWorkshifts_STG