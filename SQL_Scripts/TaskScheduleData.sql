DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\TaskScheduleData.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_TaskScheduleData_STG
FROM OPENJSON(@JSON)
WITH( 
	[TaskID] VARCHAR (250), 
	[CalendarID] VARCHAR (250), 
	[CurrentDuration] VARCHAR (250), 
	[CurrentStartDate] VARCHAR (250), 
	[CurrentFinishDate] VARCHAR (250), 
	[EarlyStartDate] VARCHAR (250), 
	[EarlyFinishDate] VARCHAR (250), 
	[LateStartDate] VARCHAR (250), 
	[LateFinishDate] VARCHAR (250), 
	[FreeFloatDuration] VARCHAR (250), 
	[TotalFloatDuration] VARCHAR (250), 
	[OnCriticalPath] VARCHAR (250), 
	[BaselineDuration] VARCHAR (250), 
	[BaselineStartDate] VARCHAR (250), 
	[BaselineFinishDate] VARCHAR (250), 
	[StartVarianceDuration] VARCHAR (250), 
	[FinishVarianceDuration] VARCHAR (250), 
	[CalculatedPercentComplete] VARCHAR (250), 
	[PhysicalPercentComplete] VARCHAR (250), 
	[RemainingDuration] VARCHAR (250), 
	[ActualStartDate] VARCHAR (250), 
	[ActualFinishDate] VARCHAR (250)
)
SELECT * FROM CA_TaskScheduleData_STG