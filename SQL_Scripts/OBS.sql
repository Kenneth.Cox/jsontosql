DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\OBS.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_OBS_STG
FROM OPENJSON(@JSON)
WITH( 
	[Level] VARCHAR (250), 
	[ID] VARCHAR (250), 
	[Name] VARCHAR (2000), 
	[SubcontractorID] VARCHAR (250),
	[ParentID] VARCHAR (250)
)
SELECT * FROM CA_OBS_STG