DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ControlAccountCustomFieldValues.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ControlAccountCustomFieldValues_STG
FROM OPENJSON(@JSON)
WITH( 
	[ControlAccountID] VARCHAR (250), 
	[CustomFieldID] VARCHAR (250), 
	[Value] VARCHAR (2000)
)
SELECT * FROM CA_ControlAccountCustomFieldValues_STG