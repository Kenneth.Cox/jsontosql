DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\CalendarExceptions.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_CalendarExceptions_STG
FROM OPENJSON(@JSON)
WITH( 
	[CalendarID]			varchar(250),
	[ExceptionDate]		varchar(250),
	[WorkHours]			varchar(250)
)
SELECT * FROM CA_CalendarExceptions_STG