DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\SummaryIndirectPerformance_ToDate.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_SummaryIndirectPerformance_ToDate_STG
FROM OPENJSON(@JSON)
WITH( 
	[SummaryIndirectElementID] VARCHAR (250), 
	[ReportingPeriodID] VARCHAR (250), 
	[BCWS_Dollars] VARCHAR (250), 
	[BCWP_Dollars] VARCHAR (250), 
	[ACWP_Dollars] VARCHAR (250)
)
SELECT * FROM CA_SummaryIndirectPerformance_ToDate_STG