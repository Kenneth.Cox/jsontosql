DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\SummaryIndirectPerformance_ToComplete.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_SummaryIndirectPerformance_ToComplete_STG
FROM OPENJSON(@JSON)
WITH( 
	[SummaryIndirectElementID] VARCHAR (250), 
	[ReportingPeriodID] VARCHAR (250), 
	[BCWS_Dollars] VARCHAR (250), 
	[EST_Dollars] VARCHAR (250)
)
SELECT * FROM CA_SummaryIndirectPerformance_ToComplete_STG