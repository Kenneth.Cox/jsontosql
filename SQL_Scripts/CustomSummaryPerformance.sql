DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\CustomSummaryPerformance.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_CustomSummaryPerformance_STG
FROM OPENJSON(@JSON)
WITH( 
	[ID] VARCHAR (250), 
	[Name] VARCHAR (250), 
	[BCWS_CumulativeToDate_Dollars] VARCHAR (250), 
	[BCWP_CumulativeToDate_Dollars] VARCHAR (250), 
	[ACWP_CumulativeToDate_Dollars] VARCHAR (250), 
	[ReprogSVA_Dollars] VARCHAR (250), 
	[ReprogCVA_Dollars] VARCHAR (250), 
	[ReprogBA_Dollars] VARCHAR (250), 
	[BAC_Dollars] VARCHAR (250), 
	[EAC_Dollars] VARCHAR (250), 
	[BCWS_CumulativeToDate_Hours] VARCHAR (250), 
	[BCWP_CumulativeToDate_Hours] VARCHAR (250), 
	[ACWP_CumulativeToDate_Hours] VARCHAR (250), 
	[ReprogSVA_Hours] VARCHAR (250), 
	[ReprogCVA_Hours] VARCHAR (250), 
	[ReprogBA_Hours] VARCHAR (250), 
	[BAC_Hours] VARCHAR (250), 
	[EAC_Hours] VARCHAR (250)
)
SELECT * FROM CA_CustomSummaryPerformance_STG