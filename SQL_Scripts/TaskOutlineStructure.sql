DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\TaskOutlineStructure.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_TaskOutlineStructure_STG
FROM OPENJSON(@JSON)
WITH( 
	[Level] VARCHAR (250), 
	[TaskID] VARCHAR (250), 
	[ParentTaskID] VARCHAR (250)
)
SELECT * FROM CA_TaskOutlineStructure_STG