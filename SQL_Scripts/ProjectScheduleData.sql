DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ProjectScheduleData.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ProjectScheduleData_STG
FROM OPENJSON(@JSON)
WITH( 
	[StatusDate] VARCHAR (250), 
	[CurrentStartDate] VARCHAR (250), 
	[CurrentFinishDate] VARCHAR (250), 
	[BaselineStartDate] VARCHAR (250), 
	[BaselineFinishDate] VARCHAR (250), 
	[ActualStartDate] VARCHAR (250),
	[ActualFinishDate]	VARCHAR (250),
	[DurationUnitsID] VARCHAR (250)
)
SELECT * FROM CA_ProjectScheduleData_STG