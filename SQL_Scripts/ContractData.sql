DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\ContractData.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_ContractData_STG
FROM OPENJSON(@JSON)
WITH( 
	[Quantity_Development] VARCHAR (250),
	[Quantity_LRIP]	VARCHAR (250),
	[Quantity_Production] VARCHAR (250),
	[Quantity_Sustainment] VARCHAR (250),
	[NegotiatedContractCost]	VARCHAR	(250),
	[AuthorizedUnpricedWork]	VARCHAR (250),
	[TargetFee] VARCHAR (250),
	[TargetPrice] VARCHAR (250),
	[EstimatedPrice] VARCHAR (250),
	[ContractCeiling] VARCHAR (250),
	[EstimatedContractCeiling] VARCHAR (250),
	[OriginalNegotiatedContractCost] VARCHAR (250),
	[ManagementEAC_BestCase] VARCHAR (250),
	[ManagementEAC_WorstCase] VARCHAR (250),
	[ManagementEAC_MostLikely] VARCHAR (250),
	[ContractBudgetBase] VARCHAR (250),
	[TotalAllocatedBudget] VARCHAR (250),
	[ContractStartDate] VARCHAR (250),
	[ContractDefinitizationDate] VARCHAR (250),
	[BaselineCompletionDate] VARCHAR (250),
	[ContractCompletionDate] VARCHAR (250),
	[ForecastCompletionDate] VARCHAR (250),
	[LastOTBDate] VARCHAR (250)
)
SELECT * FROM CA_ContractData_STG