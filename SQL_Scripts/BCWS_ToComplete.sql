DECLARE @JSON varchar(max)
SELECT @JSON = BulkColumn
    FROM OPENROWSET (BULK 'C:\Data\BCWS_ToComplete.json', SINGLE_CLOB) IMPORT
SELECT * INTO CA_BCWS_ToComplete_STG
FROM OPENJSON(@JSON)
WITH( 
	[ControlAccountID]			varchar(250),
	[WorkPackageID]				varchar(250),
	[ReportingPeriodID]			varchar(250),
	[Value_Dollars]				varchar(250),
	[Value_Dollars_Direct]		varchar(250),
	[Value_Dollars_LAB]			varchar(250),
	[Value_Dollars_LAB_Direct]	varchar(250),
	[Value_Dollars_MAT]			varchar(250),
	[Value_Dollars_MAT_Direct]	varchar(250),
	[Value_Dollars_ODC]			varchar(250),
	[Value_Dollars_ODC_Direct]	varchar(250),
	[Value_Dollars_SUB]			varchar(250),
	[Value_Dollars_SUB_Direct]	varchar(250),
	[Value_Dollars_OH]			varchar(250),
	[Value_Dollars_COM]			varchar(250),
	[Value_Dollars_GA]			varchar(250),
	[Value_Hours]					varchar(250)
)
SELECT * FROM CA_BCWS_ToComplete_STG