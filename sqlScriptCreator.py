import json
import os
import sys
from json.decoder import JSONDecodeError

class CreateFields:
    def __init__(self, json):
        self.json = json
        
    def writeColumnsSql(self):
        fieldList = []
        for fields in list(self.json):
            if type(fields) == str:
                return list(self.json)
            for dataSets in fields.keys(): 
                fieldList.append(dataSets) if dataSets not in fieldList else None
        return fieldList


class CreateSQL:
    def __init__(self, file):
        self.tableName =  file
        self.file = file + ".sql"
        
    def writer(self, jsonFile, fieldSize):
        self._setUpSQL()
        self._writeVars(jsonFile, fieldSize)
        print(")\n")
        print("SELECT * FROM " + "CA_" + self.tableName + "_STG")

    def _setUpSQL(self):
        print("DECLARE @JSON varchar(max)\n")
        print("SELECT @JSON = BulkColumn\n")
        print("    FROM OPENROWSET (BULK \'" + "C:\\Data\\" + self.file[:-3] + "json" + "\', SINGLE_CLOB) IMPORT\n")
        print("SELECT * INTO " + "CA_"  + self.tableName + "_STG"+ "\n")
        print("FROM OPENJSON(@JSON)\n")
        print("WITH( \n")

    def _writeVars(self, json, fieldSize):
        for i in json:
            print("\t"+ "[" + str(i) + "]" + " VARCHAR (" + fieldSize + ")")
            print(", \n" if str(i) is not json[len(json) - 1] else "\n")
    

class SQL:
    def __init__(self, file, stdout=None):
        self.file = file
        sys.stdout = stdout or sys.__stdout__

    def createScirpt(self, onlyJson):
        for json in onlyJson:
            print("Processing " + json)
            self.writeIfData(json)

    def writeIfData(self, tableName):
        try:
            jsonImport = json.load(open(self.file + tableName))
            fields = CreateField(jsonImport)
            CreateSQL(tableName[:-5]).writer(fields.writeColumnsSql(), "250")
        except JSONDecodeError as jsonError:
            print("Could not process: " + tableName)
            print(jsonError)


class JsonList:
    def __init__(self, file):
        self.file = file

    def buildJsonList(self):
        return [f for f in self.file if f.endswith(".json")]

if __name__ == "__main__":
    directory = "C:\Repo\jsontosqlscipt\JSON_Files\\"
    jsonList = JsonList(os.listdir(directory))  
    sql = SQL(directory)
    sql.createScirpt(jsonList.buildJsonList()) 
    
